# Equity Group Holdings
## Equity Bank
### Transaction Limit API

Using .Net Core 3.1, this project houses a simple API Controller and abstracted services for fetching data as we do not yet know the source.

Regarding authorization, jwt tokens are enabled in the config by connecting to a token service, Auth0, which has its discovery file here:

[https://equitybanktest.auth0.com/.well-known/openid-configuration](https://equitybanktest.auth0.com/.well-known/openid-configuration)

This can be easily substituted for the real auth token server in deployment. 

The API endpoints can easily be reached and reproduced in Postman via the swagger doc at runtime:

[https://localhost:44369/swagger/v1/swagger.json](https://localhost:44369/swagger/v1/swagger.json)

Finally, a temporary token has been produced for testing as follows (please let me know if you need a new one):

`eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IkR4WHI3ZU9UMGxkZUJQWXJndXNRQSJ9.eyJpc3MiOiJodHRwczovL2VxdWl0eWJhbmt0ZXN0LmF1dGgwLmNvbS8iLCJzdWIiOiJzQjJiQ0JDUXV4MU1LM2p3WkhtbWx5dzFGMllnV2U4Y0BjbGllbnRzIiwiYXVkIjoiaHR0cHM6Ly9sb2NhbGhvc3Q6NDQzNjkvc2VjdXJpdHkvYXV0aG9yaXphdGlvbiIsImlhdCI6MTU4ODg0NDM1NCwiZXhwIjoxNTg4OTMwNzU0LCJhenAiOiJzQjJiQ0JDUXV4MU1LM2p3WkhtbWx5dzFGMllnV2U4YyIsImd0eSI6ImNsaWVudC1jcmVkZW50aWFscyJ9.xHKHPEFNNXCgMOugUvjtcs0SkdVzT6FukTpnRkJsRMzKLnjlVMYpEqcEVlWeI0Yh0fL4OeDir9imOtnhyUuMLOjJfHNwUMPz-UrcJHHtGjBp12TtBXsYtEAHrsiNsde0X4mgA3D8MxJ4SB0Leg2awxmDfJPMQLR-OIFn9siOTiEmENqbDx8SRrtgkksSw7MqG6yiM2xSaJ_k2PZtaV5hBxKWVv8HdCbNytlyppRmHW-PRF5vdFmyKHqqoBEPf_ioLJzFMPy9By5Oseyxv8FonUKjnzzRtlY6r8FTpw8sN-Ru9QaUmb0hiVMScYdONYU1fOYcPjmvYbc1kqQKloO5Ww`
