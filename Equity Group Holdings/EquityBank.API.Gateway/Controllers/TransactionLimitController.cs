﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EquityBank.Models.Transaction.RequestModels;
using EquityBank.Models.Transaction.ResponseModels;
using EquityBank.Services.Transaction.TransactionLimit;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EquityBank.API.Gateway.Controllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class TransactionLimitController : ControllerBase
    {
        private readonly ITransactionLimitService _transactionLimitService;
        public TransactionLimitController(ITransactionLimitService transactionLimitService)
        {
            // TO DO: add DI for HTTPContext and logging
            _transactionLimitService = transactionLimitService;
        }
        

        [HttpPost("/v1/globallimit")]
         public GetLimitResponseParameters CreateGlobalLimit([FromBody] CreateGlobalLimitRequestParameters requestParameters)
        {
            return _transactionLimitService.CreateGlobalTransactionLimit(requestParameters);
        }

        [HttpPost("/v1/transactionlimit")]
        public GetLimitResponseParameters CreateTransactionLimit([FromBody] CreateLimitRequestParameters requestParameters)
        {
            return _transactionLimitService.CreateTransactionLimit(requestParameters);
        }

        [HttpPost("/v1/getlimit")]
        public GetLimitResponseParameters GetTransactionLimit([FromBody] GetLimitRequestParameters requestParameters)
        {
            return _transactionLimitService.GetTransactionLimit(requestParameters);
        }
    }
}
