﻿using EquityBank.API.Gateway.Controllers;
using EquityBank.Services.Transaction.TransactionLimit;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EquityBank.API.Gateway.Tests.Controllers
{
    public class TansactionLimitControllerTests
    {
        TransactionLimitController _controller;
        ITransactionLimitService _service;

        public TansactionLimitControllerTests()
        {
            _service = new TestTransactionLimitService();
            _controller = new TransactionLimitController(_service);
        }

        [Fact]
        public void Test_TestCreateGlobalLimit_Returns_OK()
        {
            _service = new TestTransactionLimitService();
            var okResult = _controller.CreateGlobalLimit(
                new Models.Transaction.RequestModels.CreateGlobalLimitRequestParameters
                {
                    rid = "1",
                    productType = "sendToMpesa",
                    dailyLimit = "1000000",
                    weeklyLimit = "1000000",
                    monthlyLimit = "1000000",
                    yearlyLimit = "1000000",
                    transactionLimit = "3000",
                    schemeCode = "LA324",
                    channel = "Way4",
                    countryCode = "KE",
                    currency = "KES",
                    bankCode = "54"
                });
            Assert.Equal("200",okResult.ToString());
        }

        [Fact]
        public void Test_TestCreateTransactionLimit_Returns_OK()
        {
            _service = new TestTransactionLimitService();
            var okResult = _controller.CreateTransactionLimit(
                new Models.Transaction.RequestModels.CreateLimitRequestParameters
                {
                    rid = "1",
                    accountId = "01701934343512",
                    customerType = "individual",
                    productType = "sendToMpesa",
                    dailyLimit = "1000000",
                    weeklyLimit = "1000000",
                    monthlyLimit = "1000000",
                    yearlyLimit = "1000000",
                    transactionLimit = "3000",
                    schemeCode = "LA324",
                    channel = "Way4",
                    countryCode = "KE",
                    currency = "KES",
                    bankCode = "54"
                });
            Assert.Equal("200", okResult.status.code);
        }
    }
}
