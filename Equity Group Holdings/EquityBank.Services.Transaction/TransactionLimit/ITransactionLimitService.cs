﻿using EquityBank.Models.Transaction.RequestModels;
using EquityBank.Models.Transaction.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace EquityBank.Services.Transaction.TransactionLimit
{
    public interface ITransactionLimitService
    {
        GetLimitResponseParameters CreateGlobalTransactionLimit(CreateGlobalLimitRequestParameters requestParams);
        GetLimitResponseParameters CreateTransactionLimit(CreateLimitRequestParameters requestParams);
        GetLimitResponseParameters GetTransactionLimit(GetLimitRequestParameters requestParams);
    }
}
