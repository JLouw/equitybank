﻿using EquityBank.Models.Transaction.RequestModels;
using EquityBank.Models.Transaction.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace EquityBank.Services.Transaction.TransactionLimit
{
    public class TestTransactionLimitService : ITransactionLimitService
    {
        public GetLimitResponseParameters CreateGlobalTransactionLimit(CreateGlobalLimitRequestParameters requestParams)
        {
            return new GetLimitResponseParameters
            {
                rid = requestParams.rid,
                status = new StatusResponse
                {
                    code = "200",
                    description = "Global Test successful!"
                },
                limit = new LimitResponse
                {
                    dailyLimit = requestParams.dailyLimit,
                    weeklyLimit = requestParams.weeklyLimit,
                    monthlyLimit = requestParams.monthlyLimit,
                    yearlyLimit = requestParams.yearlyLimit,
                    transactionLimit = requestParams.transactionLimit
                }
            };
        }

        public GetLimitResponseParameters CreateTransactionLimit(CreateLimitRequestParameters requestParams)
        {
            return new GetLimitResponseParameters
            {
                rid = requestParams.rid,
                status = new StatusResponse 
                { 
                    code = "200", 
                    description = "Test successful for account ["+requestParams.accountId.ToString()+"]!" 
                },
                limit = new LimitResponse 
                { 
                    dailyLimit = requestParams.dailyLimit, 
                    weeklyLimit = requestParams.weeklyLimit, 
                    monthlyLimit = requestParams.monthlyLimit, 
                    yearlyLimit = requestParams.yearlyLimit, 
                    transactionLimit = requestParams.transactionLimit}
            };
        }

        public GetLimitResponseParameters GetTransactionLimit(GetLimitRequestParameters requestParams)
        {
            return new GetLimitResponseParameters
            {
                rid = requestParams.rid,
                status = new StatusResponse
                {
                    code = "200",
                    description = "Test successful!"
                },
                limit = new LimitResponse
                {
                    dailyLimit = "10000",
                    weeklyLimit = "10000",
                    monthlyLimit = "10000",
                    yearlyLimit = "10000",
                    transactionLimit = "10000"
                }
            };
        }
    }
}
