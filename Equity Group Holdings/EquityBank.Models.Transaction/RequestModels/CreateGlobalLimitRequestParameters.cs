﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EquityBank.Models.Transaction.RequestModels
{
    public class CreateGlobalLimitRequestParameters
    {
        public string rid { get; set; }
        public string productType { get; set; }
        public string dailyLimit { get; set; }
        public string weeklyLimit { get; set; }
        public string monthlyLimit { get; set; }
        public string yearlyLimit { get; set; }
        public string transactionLimit { get; set; } //there is a mis-spelling is in the brief document
        public string schemeCode { get; set; }
        public string channel { get; set; }
        public string countryCode { get; set; }
        public string currency { get; set; }
        public string bankCode { get; set; }
    }
}
