﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EquityBank.Models.Transaction.RequestModels
{
    public class GetLimitRequestParameters
    {
        public string rid { get; set; }
        public string accountId { get; set; }
        public string schemeCode { get; set; }
        public string channel { get; set; }
        public string countryCode { get; set; }
        public string currency { get; set; }
        public string bankCode { get; set; }
    }
}
