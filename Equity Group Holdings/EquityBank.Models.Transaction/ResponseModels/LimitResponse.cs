﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EquityBank.Models.Transaction.ResponseModels
{
    public class LimitResponse
    {
        public string dailyLimit { get; set; }
        public string weeklyLimit { get; set; }
        public string monthlyLimit { get; set; }
        public string yearlyLimit { get; set; }
        public string transactionLimit { get; set; }
    }
}
