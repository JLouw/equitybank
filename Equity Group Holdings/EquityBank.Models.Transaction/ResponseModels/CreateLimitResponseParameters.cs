﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EquityBank.Models.Transaction.ResponseModels
{
    public class CreateLimitResponseParameters
    {
        public string rid { get; set; }
        public StatusResponse status { get; set; }
        public LimitResponse limit { get; set; }
    }
}
