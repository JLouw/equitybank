﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EquityBank.Models.Transaction.ResponseModels
{

    public class StatusResponse
    {
        public StatusResponse(string code)
        {
            this.code = code;
            //Add enum or config lookup if necessary
        }
        public StatusResponse()
        {
            //empty
        }
        public string code { get; set; }
        public string description { get; set; }
    }
}
